import React from "react";
import { Routes, Route,  } from "react-router-dom";
import HomePage from "./pages/HomePage";
import MainLayout from "./layouts/MainLayout";
import NotFound from "./pages/NotFound";
import TodoPage from "./pages/TodoPage";
import ProductsPage from "./pages/Productspage";

const App: React.FC = ( ) => (
    <Routes>
        <Route path="/" element={<MainLayout />}>
            <Route index element={<HomePage />} />
            <Route path="/products/:id/:slug" element={<ProductsPage />} />
            <Route path="/todo" element={<TodoPage />} />
            <Route path="*" element={<NotFound />} />
        </Route>
    </Routes>
);


export default App;