import React, { memo, useEffect, useState } from "react";
import { IAttachments, IPhoto } from "../../types/data";

import "./cart-product.scss"

interface IComponentProduct extends IPhoto {
    className: string;
}

interface IThumbnail {
    url: string;
    text: string;
}

const CartProduct: React.FC<IComponentProduct> = (props) => {
    const [art, setArt] = useState("");
    const [title, setTile] = useState("");
    const [thumbnail, setThumbnail] = useState<IThumbnail>({ url: "", text: "" });

    useEffect(() => {
        const { text, sizes } = props;

        const url = sizes.find(size => size.type === "x")?.url || "";

        setThumbnail({
            url,
            text: ''
        });

        let textArray = text.split("Цена:");
        if (textArray.length > 0) {
            setTile(textArray[0]?.trim());
            setArt(textArray[1]?.trim());
        }
    }, [props.text]);

    return (
        <div className="cart-product">
            <h2>{title}</h2>
            <div className="cart-product__thumbnail">
                <img
                    src={thumbnail.url}
                />
            </div>
            <b>Цена: {art}</b>
        </div>
    )
};

export default memo(CartProduct, () => (true))
