import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { transliterate } from "../../helpers/transliterate";
import { RootState } from "../../store";

import { fetchCategoriesList } from "../../store/categoriesSlice";

import "./categories.scss"


// https://api.vk.com/method/board.getTopics?v=5.131&group_id=5552499&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0   -категории
// https://api.vk.com/method/board.getComments?v=5.131&group_id=5552499&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0&topic_id=37143725 -продукты
// https://vk.com/topic-5552499_37143725
//https://vk.com/dev/versions
//https://dev.vk.com/method/board.getComments#%D0%9F%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B
//https://dev.vk.com/reference/objects/comment-topic
//https://vk.com/editapp?id=6384895&section=options


const Categories: React.FC = () => {
    const dispatch = useDispatch();

    const {
        categoriesInfo
    } = useSelector((state: RootState) => {
        return {
            categoriesInfo: state.categoriesSlice
        }
    })

    useEffect(() => {
        const { categories } = categoriesInfo;
        if (categories.length === 0) {
            dispatch(fetchCategoriesList())
        }
    }, []);
 
    const { categories, isLoading } = categoriesInfo

    return (
        <div className="categories">
            {!isLoading &&
                categories.map(item => (
                    <Link
                        key={item.id}
                        className="categories__link"
                        to={`/products/${item.id}/${transliterate((item.title).toLowerCase())}`} >
                        {item.title}
                    </Link>
                ))
            }
        </div>
    )
};

export default Categories
