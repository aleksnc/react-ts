import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../../store/todoSlice";

import TodoList from "../TodoList";
import "./increment.scss"

const Increment: React.FC = () => {
    const dispatch = useDispatch()
    const [value, setValue] = useState('');
    const inputRef = useRef<HTMLInputElement>(null);

    const handleInput: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setValue(e.target.value)
    }

    const handleEnter: React.KeyboardEventHandler<HTMLInputElement> = (e) => {
        if (e.key === 'Enter') {
            handleAddTodoBtn();
        }
    }

    const handleAddTodoBtn = () => {
        if (value) {
            dispatch(addTodo(value))
            setValue('')
        }
    }

    useEffect(() => {
        if (inputRef?.current) {
            inputRef.current.focus();
        }
    }, [])

    return (
        <div className="increment">
            <input
                ref={inputRef}
                type="text"
                value={value}
                onChange={handleInput}
                onKeyDown={handleEnter}
            />
            <button
                onClick={handleAddTodoBtn}
            >
                Add
            </button>

            <br />
            <br />

            <TodoList />
        </div>
    )
};

export default Increment
