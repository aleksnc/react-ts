import React from "react";

import "./title.scss"

const Title: React.FC<{title: string}> = (props) => {
    const { title } = props;
    return (
        <h1 className="title">{title}</h1>
    )
};

export default Title
