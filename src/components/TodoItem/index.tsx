import React, { memo } from "react";

import { ITodo } from "../../types/data"
import "./todo-item.scss"

interface ITodoItem extends ITodo {
    toggleTodo: (id: number) => void;
    removeTodo: (id: number) => void;
}

const TodoItem: React.FunctionComponent<ITodoItem> = (props) => {
    const { id, title, complete, toggleTodo, removeTodo } = props;

    return (
        <div className="todo-item">
            <input
                type="checkbox"
                checked={complete}
                onChange={() => toggleTodo(id)}
            />
            {title}
            <button
                className="todo-item__btn"
                onClick={() => removeTodo(id)}
            >
                Remove
            </button>
        </div>
    )
}

const areEqual = (prevProps: ITodoItem, nextProps: ITodoItem) => {
    return nextProps.complete === prevProps.complete;
}

export default memo(TodoItem, areEqual);