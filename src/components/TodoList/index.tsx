import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../store";
import { removeTodo, toggleTodoComplete } from "../../store/todoSlice";

import TodoItem from "../TodoItem";
import "./todo-list.scss"

const TodoList: React.FC = () => {
    const dispatch = useDispatch();

    const handleRemove = (id: number) => {
        dispatch(removeTodo(id));
    }

    const handleToggle = (id: number) => {
        dispatch(toggleTodoComplete(id))
    }

    const {
        todos
    } = useSelector((state: RootState) => {
        return {
            todos: state.todoSlice.todos,
            products: state.productsSilce.products
        }
    })
 



    return (
        <div className="todo-list">
            {todos.map(todo => (
                <TodoItem
                    removeTodo={handleRemove}
                    toggleTodo={handleToggle}
                    key={todo.id}
                    {...todo}
                />
            ))}
 
        </div>
    )
}

export default TodoList;