export  interface Icontent {
    method?: string,
    url: string,
    data?: string,
    error?: (err: object) => void;
}

export const fetchAPI = async (content: Icontent) => {
    const { method = 'get', url, data='', error = () => { } } = content
    if (method === 'get') {
        return await fetch(url)
            .then(res => res.json())
            .catch((err) => { error({ err }) })
    }

}