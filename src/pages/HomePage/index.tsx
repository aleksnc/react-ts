import React from "react";
import Categories from "../../components/Categories"; 
import Title from "../../components/Title";

import "./home-page.scss"

const HomePage = () => {

    return (
        <div className="home-page">
            <Title title="My Home Page" /> 
            <Categories />
        </div>

    )
};

export default HomePage
