import React from "react";

import "./not-found.scss"

const NotFound = () => (
    <h1 className="not-found">Page not found</h1>
);

export default NotFound
