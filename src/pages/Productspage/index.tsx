import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState } from "../../store";

import { fetchProductsList } from "../../store/productsSilce";
import Title from "../../components/Title";

import "./products-page.scss"
import CartProduct from "../../components/CartProduct";

const ProductsPage: React.FC = (props) => {
    const dispatch = useDispatch();
    const params = useParams();

    const {
        productsInfo
    } = useSelector((state: RootState) => {
        return {
            productsInfo: state.productsSilce
        }
    })

    useEffect(() => {
        const { products } = productsInfo;

        if (products.length === 0 && params.id) {
            dispatch(fetchProductsList({ id: params.id }))
        }
    }, []);

    useEffect(() => {
        const { products } = productsInfo;

        if (products.length !== 0 && params.id) {
            dispatch(fetchProductsList({ id: params.id }))
        }
    }, [params.id]);

    const { products, isLoading } = productsInfo;

    return (
        <div className="products-page">
            <Title title="My Products Page" />
            <div className="products-page__listAll">
                {!isLoading && products.map(product => (
                    <>
                        <h2>{product.text}</h2>
                        <div className="products-page__list">
                            {product.attachments.map(attach => {
                                if (attach.type === "photo") {
                                    return (
                                        <CartProduct
                                            className="products-page__cart"
                                            key={attach.photo.id}
                                            {...attach.photo}
                                        />
                                    )
                                }
                            })}
                        </div>
                    </>
                ))}
            </div>
        </div>
    )
};

export default ProductsPage
