import React from "react";
import Increment from "../../components/Increment";
import Title from "../../components/Title";

import "./todo-page.scss"

const TodoPage = () => {
    return (
        <div className="todo-page">
            <Title title="My Todo Page" />
            <Increment />
        </div>

    )
};

export default TodoPage
