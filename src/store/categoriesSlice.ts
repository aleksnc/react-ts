import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchAPI } from "../helpers/fetchAPI";
import { ICategory } from "../types/data"

export interface IItems {
    items: ICategory[];
}

export interface IResponse {
    response: IItems,
    error?: object
};

export const fetchCategoriesList = createAsyncThunk<IResponse, void, {}>(
    'categoriesList/fetchList',
    async (userData, thunkAPI) => {
        const response: IResponse = await fetchAPI(
            {
                url: "/method/board.getTopics?v=5.131&group_id=5552499&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0",
                error: (err) => {
                    return thunkAPI.rejectWithValue(err)
                }
            }
        );

        if (response?.error) {
            return thunkAPI.rejectWithValue(response)
        }

        return response;
    }
)

const categoriesSilce = createSlice({
    name: 'categoriesList',
    initialState: {
        categories: [] as ICategory[],
        isLoading: true,
        errors: null
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchCategoriesList.pending, (state) => {
                state.errors = null;
                state.isLoading = true;
            })
            .addCase(fetchCategoriesList.fulfilled, (state, action: PayloadAction<IResponse>) => {
                const { response: { items } } = action.payload;
                state.categories = items;
                state.isLoading = false;
            })
            .addCase(fetchCategoriesList.rejected, (state, action) => {
                console.log('state, action rejected', state, action)
            })
    }
})

export const {  } = categoriesSilce.actions
export default categoriesSilce.reducer;