import { configureStore } from '@reduxjs/toolkit'
import todoSlice from './todoSlice'; 
import productsSilce from './productsSilce'; 
import categoriesSlice from './categoriesSlice'; 

export const store = configureStore({
  reducer: {
    todoSlice,
    productsSilce,
    categoriesSlice
  },
})
 
export type RootState = ReturnType<typeof store.getState> 
export type AppDispatch = typeof store.dispatch 