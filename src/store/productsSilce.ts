import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchAPI } from "../helpers/fetchAPI";
import { IProduct } from "../types/data"

export interface IItems {
    items: IProduct[];
}

export interface IResponse {
    response: IItems,
    error?: object
};

export interface IUserData {
    id: string
};


export const fetchProductsList = createAsyncThunk<IResponse, IUserData, {}>(
    'productsList/fetchList',
    async (userData, thunkAPI) => {
        const {id} = userData;
 
        const response: IResponse = await fetchAPI(
            {
                url: `/method/board.getComments?v=5.131&group_id=5552499&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0&topic_id=${id}`,
                error: (err) => {
                    return thunkAPI.rejectWithValue(err)
                }
            }
        );

        if (response?.error) {
            return thunkAPI.rejectWithValue(response)
        }

        return response;
    }
)

const productsSilce = createSlice({
    name: 'productsList',
    initialState: {
        products: [] as IProduct[],
        isLoading: false,
        errors: null
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchProductsList.pending, (state) => {
                state.errors = null;
                state.isLoading = true;
            })
            .addCase(fetchProductsList.fulfilled, (state, action: PayloadAction<IResponse>) => {
                const { response: { items } } = action.payload;
                state.products = items;
                state.isLoading = false;
            })
            .addCase(fetchProductsList.rejected, (state, action) => {
                console.log('state, action rejected', state, action)
            })
    }
})

export const {  } = productsSilce.actions
export default productsSilce.reducer;