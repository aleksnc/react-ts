import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ITodo } from "../types/data"

const todoSlice = createSlice({
    name: 'todos',
    initialState: {
        todos: [] as ITodo[]
    },
    reducers: {
        addTodo(state, action: PayloadAction<string>) {
            state.todos.push({
                id: Date.now(),
                title: action.payload,
                complete: false
            })
        },
        removeTodo(state, action: PayloadAction<number>) {
            state.todos = state.todos.filter(todo => todo.id !== action.payload)
        },
        toggleTodoComplete(state, action: PayloadAction<number>) {
            state.todos = state.todos.map((todo) => {
                if (todo.id !== action.payload) {
                    return todo
                }

                return {
                    ...todo,
                    complete: !todo.complete
                }
            })
        }
    }
})

export const { addTodo, removeTodo, toggleTodoComplete } = todoSlice.actions
export default todoSlice.reducer;