export interface ITodo {
    id: number;
    title: string;
    complete: boolean;
}

export interface IPhotoSizes {
    height: number,
    type: string,
    url: string,
    width: number
}

export interface IPhoto{
    sizes: IPhotoSizes[];
    date: number;
    text: string;
    id: string;
}

export interface IAttachments {
    type: string;
    photo: IPhoto
}

export interface ICategory {
    id: number;
    title: string;
    created: number;
}

export interface IProduct {
    id: number;
    text: string;
    date: number;
    attachments: IAttachments[]
}
