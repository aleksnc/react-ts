import path from "path";
import { Configuration } from "webpack";
const ip = require('ip');
const DEV_IP_ADDR = ip.address();
const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
    entry: "./src/index.tsx",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js",
    },
    devServer: {
        static: path.join(__dirname, "build"),
        port: 5004,
        host: DEV_IP_ADDR,
        historyApiFallback: true,
        hot: true,
        proxy: { // for api requests
            '/method/**': {
                target: 'https://api.vk.com/',
                secure: false,
                changeOrigin: true,
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
            },
            {
                test: /\.(ts|js)x?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "@babel/preset-env",
                            "@babel/preset-react",
                            "@babel/preset-typescript",
                        ],
                    },
                },
            },
            {
                test: /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/,
                use: ['file-loader']
            }
        ],
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
}; 